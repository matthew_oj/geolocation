<?php

namespace Matthewoj\Geolocation;

use Illuminate\Support\ServiceProvider;

class GeolocationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->publishes([
            __DIR__.'/../resources/assets/js/components' => base_path('resources/assets/js/components/geolocation'),
        ], 'geolocation-components');

        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations'),
        ], 'location-migrations');

        $this->publishes([
            __DIR__.'/config.php' => config_path('location.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/GeolocationRules.php' => base_path('/app/Http/Requests/GeolocationRules.php'),
        ], 'config');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // controllers
        $this->app->make('Matthewoj\Geolocation\GeolocationRules');
        $this->app->make('Matthewoj\Geolocation\GeolocationController');
    }
}
