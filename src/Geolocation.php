<?php

namespace Matthewoj\Geolocation;

use Illuminate\Database\Eloquent\Model;

class Geolocation extends Model
{
    protected $table = 'geolocation';
    
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    
    protected $fillable = [
        'latitude',
        'longitude'
    ];

   /* Query the given table directly with a specified Latitude & Longitude
		:POTENTIAL FOR GLOBAL SCOPE
	*/
    public function scopeDistance( $query, $lat, $long, $format )
    {
        $miles  = 3959;
        $kms    = 6371;

        $formatting = $format === 'miles' ? $miles : $kms;

        $raw = \DB::raw('ROUND ( ( '. $formatting . ' * acos( cos( radians('. $lat .') ) * cos( radians( geolocation.latitude ) ) * cos( radians( geolocation.longitude ) - radians( '. $long .' ) ) + sin( radians( '. $lat .' ) ) * sin( radians( geolocation.latitude ) ) ) ) ) AS distance');

        return $query->select('*')
            ->addSelect( $raw )
            ->orderBy( 'distance', 'ASC' );
    }
}
