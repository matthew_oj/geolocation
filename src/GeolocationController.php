<?php

namespace Matthewoj\Geolocation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeolocationController extends Controller
{
    protected $format;

    function __construct() {
        $this->format = config('location.default')['calculation'];
    }

    public function index( $id = null ) 
    {
        return $id === null ? Geolocation::where('id', '>=', 0)->get() : Geolocation::findOrFail($id)->get();
    }

    public function distance( $lat, $long, $id = null ) {
        return $id === null ? Geolocation::where('id', '>=', 0)->distance($lat, $long, $this->format)->get() : Geolocation::findOrFail($id)->distance($lat, $long, $this->format)->get();
    }

    public function format() {
        return $this->format;
    }

    public function create(GeolocationRules $request)
    {
        return Geolocation::create([
            'latitude' => $request->input('latitude'),
            'longitude' => $request->input('longitude')
        ]);
    }

    public function update(Request $request)
    {
        return Geolocation::findOrFail($request->input('id'))
            ->update([
                'latitude' => $request->input('latitude'),
                'longitude' => $request->input('longitude')
            ]);
    }

    public function remove($id)
    {
        return Geolocation::destroy($id);
    }
}
