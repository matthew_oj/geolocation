<?php

Route::get('location_format', ['uses' => 'Matthewoj\Geolocation\GeolocationController@format']);
Route::get('location', ['uses' => 'Matthewoj\Geolocation\GeolocationController@index']);
Route::get('location/{id}', ['uses' => 'Matthewoj\Geolocation\GeolocationController@index']);
Route::get('location/{lat}/{long}', ['uses' => 'Matthewoj\Geolocation\GeolocationController@distance']);
Route::get('location/{id}/{lat}/{long}', ['uses' => 'Matthewoj\Geolocation\GeolocationController@distance']);
Route::post('location', ['uses' => 'Matthewoj\Geolocation\GeolocationController@create']);
Route::put('location', ['uses' => 'Matthewoj\Geolocation\GeolocationController@update']);
Route::delete('location/{id}', ['uses' => 'Matthewoj\Geolocation\GeolocationController@remove']);