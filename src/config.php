<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Location Defaults
    |--------------------------------------------------------------------------
    |
    | This option controlls the calculation method for distances,
    / Options include:
    / - 'kms': for distance calculated in Kilometers
    / - 'miles': for distance calculated in Miles
    */

    'default' => [
        'calculation' => 'kms',
        //'calculation' => 'miles',
    ]
];